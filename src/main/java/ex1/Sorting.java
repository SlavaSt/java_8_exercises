/* 3/31/14 */
package ex1;

import java.util.Comparator;

import static util.Out.println;

/**
 * @author Slava Stashuk
 */
public class Sorting {

    public static <T extends Comparable<T>> void msort(T[] array) {
        msort(array, Comparator.naturalOrder());
    }

    public static <T> void msort(T[] array, Comparator<T> comparator) {
        new MergeSort<>(array, comparator).sort();
    }

    public static void main(String[] args) {
        Integer[] arr = {2, 6, 9, 1, 6, 7, 7, 10, 11, 2 ,3 ,4,};
        Integer[] sorted = new MergeSort<>(arr, Comparator.naturalOrder()).sort();
        println(arr);
    }

    static class MergeSort<T> {

        final Comparator<T> comparator;
        final T[] array, auxiliary;

        @SuppressWarnings("unchecked")
        MergeSort(T[] array, Comparator<T> comparator) {
            this.comparator = comparator;
            this.array = array;
            this.auxiliary = (T[]) new Object[array.length];
        }

        T[] sort() {
            sort(0, array.length >> 1, array.length - 1);
            return array;
        }

        void sort(int lo, int mid, int hi) {
            if (lo < mid) sort(lo, (lo + mid) >> 1, mid);
            if (mid + 1 < hi) sort(mid + 1, (hi + mid + 1) >> 1, hi);
            merge(lo, mid + 1, hi + 1);
        }

        void merge(int lo, int mid, int hi) {
            for (int i = lo, j = lo, k = mid; i < hi; i++) {
                if (k >= hi || j < mid && comparator.compare(array[j], array[k]) <= 0) auxiliary[i] = array[j++];
                else auxiliary[i] = array[k++];
            }
            System.arraycopy(auxiliary, lo, array, lo, hi - lo);
        }
    }
}
