/* 5/10/14 */
package princeton;


/**
 * @author Kolya Panchyshyn, Slava Stashuk
 */
public class DutchFlag {

    int[] arr;
    int colorCount = 0, swapCount = 0;

    void sort() {
        int pivot = 1;
        int i = 0, j = 0, k = arr.length - 1;

        int colorI = -1; // value not used
        while (true) {

            while (i <= k && (colorI = color(i)) < pivot) // Swaps zeroes
                swap(i++, j++);

            if (i >= k) break;

            if (colorI == pivot)
                i++;
            else  // colorI > pivot
                swap(i, k--);
        }
    }

    int color(int i) {
        colorCount++;
        return arr[i];
    }

    void swap(int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
        swapCount++;
    }
}
