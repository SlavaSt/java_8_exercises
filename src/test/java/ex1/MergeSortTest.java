/* 3/31/14 */
package ex1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static com.google.common.collect.Lists.newArrayList;
import static ex1.Sorting.msort;
import static org.junit.Assert.assertArrayEquals;
import java.util.Random;


/**
 * @author Slava Stashuk
 */
@RunWith(Parameterized.class)
public class MergeSortTest {

    static Random random = new Random();

    final Integer[] randomArray;

    public MergeSortTest(Integer[] randomArray) {
        this.randomArray = randomArray;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> randomArrays() {
        Collection<Object[]> arrays = newArrayList();
        for (int i = 0; i < 100; i++) {
            Integer[] array = new Integer[random.nextInt(100)];
            for (int j = 0; j < array.length; j++) {
                array[j] = random.nextInt();
            }
            arrays.add(new Object[] {array});
        }
        return arrays;
    }

    @Test
    public void testMerge() throws Exception {
        Integer[] expected = randomArray.clone();
        Arrays.sort(expected);
        Integer[] actual = randomArray.clone();
        msort(actual);
        assertArrayEquals(expected, actual);
    }
}
